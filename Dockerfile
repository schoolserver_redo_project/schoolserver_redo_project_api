FROM python:3.7


EXPOSE 80


RUN pip3 install gunicorn


RUN pip3 install git+https://gitlab.com/schoolserver_redo_project/iservscrapping/


COPY ./ /app
WORKDIR /app

RUN pip3 install -r requirements.txt

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

CMD gunicorn --bind 0.0.0.0:80 wsgi
