# Iserv Redo - Server
This is a Flask application serving a REST API for an Untis substition plan using iservscrapper
## Dependencies:
 * Flask
 * iservscrapping (https://gitlab.com/Niwla23/iservscrapping)
 * Flask-Cors
 * Flask-Jsonpify

## Setup:
 1. Copy the config.json.example to config.json
 2. Change the values in config.json
 3. Do flask run --host 0.0.0.0 --port 5000 (or if you have too much time set up a production server)

## Contributing
Contributing to this project is very welcome!
By sending a PR, you agree that your code is licensed under MIT by me. You will be listed as Contributor in the Repos info.

## Contributors
 * Niwla23
